package com.final_project.smartsignature;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class ActionStatus {
    private String userToken;
    private ActionsEnum actionStatus;
    private String macAddress;

    public ActionStatus(){
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public ActionStatus(String userToken, ActionsEnum actionStatus, String macAddress) {
        this.userToken = userToken;
        this.actionStatus = actionStatus;
        this.macAddress = macAddress;
    }

    public String getUserToken() {
        return userToken;
    }

    public ActionsEnum getActionStatus() {
        return actionStatus;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setActionStatus(ActionsEnum actionStatus) {
        this.actionStatus = actionStatus;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("userToken", userToken);
        result.put("actionStatus", actionStatus);
        result.put("macAddress", macAddress);

        return result;
    }
}
