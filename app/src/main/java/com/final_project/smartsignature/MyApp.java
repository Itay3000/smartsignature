package com.final_project.smartsignature;

import android.app.Application;
import android.bluetooth.BluetoothDevice;

import com.final_project.smartsignature.bluetooth_handlers.ConnectThread;

public class MyApp extends Application {
    private ConnectThread mConnectThread;
    public String userToken;
    public String macAddress;

    @Override
    public void onCreate() {
        super.onCreate();
        //mConnectThread = new ConnectThread();
    }

    public ConnectThread newConnectionThreadInstance(BluetoothDevice bluetoothDevice){
        mConnectThread =  new ConnectThread(bluetoothDevice);
        return mConnectThread;
    }

    public ConnectThread getConnectionThreadInstance() {
        if (mConnectThread == null)
            throw new RuntimeException("Called getConnectionThreadInstance before initialize instance");
        return mConnectThread;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        if(userToken != null && macAddress != null) {
            ReadWriteSnippets snippets = new ReadWriteSnippets(this);
            snippets.deleteAllMessages(userToken);
            snippets.writeAction(new ActionStatus(userToken, ActionsEnum.DISCONNECT, macAddress));
        }
    }
}
