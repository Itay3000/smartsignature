package com.final_project.smartsignature;

import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import finger_print_auth.AuthErrorCodes;
import finger_print_auth.FingerPrintAuthCallback;
import finger_print_auth.FingerPrintAuthHelper;
import finger_print_auth.FingerPrintUtils;

public class FingerPrintAuthActivity extends AppCompatActivity  implements FingerPrintAuthCallback {

    private static final String MSG_KEY = "MSG_KEY";
    private static final String BUNDLE_KEY = "BUNDLE_KEY";
    public static final String TAG = "FingerPrintAuthActivity";

    private TextView mAuthMsgTv;
    private ViewSwitcher mSwitcher;
    private Button mGoToSettingsBtn;
    private FingerPrintAuthHelper mFingerPrintAuthHelper;

    private Bundle appBundle;
    private  String fromActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finger_print_auth);

        appBundle = getIntent().getExtras();
        if(appBundle.getString(ActionActivity.ACTIVITY) != null){
            fromActivity = appBundle.getString(ActionActivity.ACTIVITY);
        }else
            fromActivity = "";

        mGoToSettingsBtn = (Button) findViewById(R.id.go_to_settings_btn);
        mGoToSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FingerPrintUtils.openSecuritySettings(FingerPrintAuthActivity.this);
            }
        });

        mSwitcher = (ViewSwitcher) findViewById(R.id.main_switcher);
        mAuthMsgTv = (TextView) findViewById(R.id.auth_message_tv);

        mFingerPrintAuthHelper = FingerPrintAuthHelper.getHelper(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGoToSettingsBtn.setVisibility(View.GONE);

        mAuthMsgTv.setText("Scan your finger");

        //start finger print authentication
        mFingerPrintAuthHelper.startAuth();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mFingerPrintAuthHelper.stopAuth();
    }

    @Override
    public void onNoFingerPrintHardwareFound() {
        Log.e(TAG, "onNoFingerPrintHardwareFound: Doesn't support fingerprint");
        doesntSupportDialog(R.string.authentication_failed);
    }

    @Override
    public void onNoFingerPrintRegistered() {
        mAuthMsgTv.setText(getString(R.string.fingerprint_not_registered));
        mGoToSettingsBtn.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBelowMarshmallow() {
        Log.e(TAG, "onBelowMarshmallow: Doesn't support fingerprint");

        doesntSupportDialog(R.string.you_running_old_version);
    }

    private void doesntSupportDialog(int msg) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.not_compatible)
                .setMessage(msg)
                .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create().show();
    }

    @Override
    public void onAuthSuccess(FingerprintManager.CryptoObject cryptoObject) {
        Toast.makeText(FingerPrintAuthActivity.this, "Authentication succeeded.", Toast.LENGTH_SHORT).show();

        Bundle bundle = new Bundle();
        //To add the MacAddress and userToken
        bundle.putString(MainActivity.USER_TOKEN,  getIntent().getExtras().getString(MainActivity.USER_TOKEN));

        if(fromActivity .equals( ActionActivity.TAG)){
            bundle.putString(ActionActivity.ACTIVITY, TAG);
            bundle.putString(MainActivity.MAC_ADDRESS, getIntent().getExtras().getString(MainActivity.MAC_ADDRESS));
            startActivity(new Intent(FingerPrintAuthActivity.this, ActionActivity.class)
                    .putExtras(bundle));
        }
        else
            startActivity(new Intent(FingerPrintAuthActivity.this, BluetoothActivity.class).putExtras(bundle));
    }

    @Override
    public void onAuthFailed(int errorCode, String errorMessage) {
        switch (errorCode) {
            case AuthErrorCodes.CANNOT_RECOGNIZE_ERROR:
                mAuthMsgTv.setText("Cannot recognize your finger print. Please try again.");
                break;
            case AuthErrorCodes.NON_RECOVERABLE_ERROR:
                mAuthMsgTv.setText("Cannot initialize finger print authentication. Please try again.");
                break;
            case AuthErrorCodes.RECOVERABLE_ERROR:
                mAuthMsgTv.setText(errorMessage);
                break;

        }
    }
}