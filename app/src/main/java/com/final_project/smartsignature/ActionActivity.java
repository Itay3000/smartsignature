package com.final_project.smartsignature;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.final_project.smartsignature.bluetooth_handlers.ConnectThread;
import com.final_project.smartsignature.bluetooth_handlers.DeviceItem;

public class ActionActivity extends AppCompatActivity {
    final static String TAG="ActionActivity";
    public final static String ACTIVITY = "Activity";
    public final static String AUTH = "Auth";
    ReadWriteSnippets snippets;
    String userToken;
    String macAddress;
    private static TextView actionText;
    private String mLastActivity;
    private static Context mContext;
    private static ProgressBar progressBar;
    private static ConnectThread connectThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action);
        mContext = this.getApplicationContext();
        actionText = findViewById(R.id.action_message);
        progressBar = findViewById(R.id.progress_bar);
        //init info from last activity
        Bundle bundle = getIntent().getExtras();

        if(bundle==null)
            Log.e(TAG, "onCreate: Cannot open bundle");

        userToken = bundle.getString(MainActivity.USER_TOKEN);
        macAddress = bundle.getString(MainActivity.MAC_ADDRESS);

        ((MyApp)getApplicationContext()).userToken = userToken;
        ((MyApp)getApplicationContext()).macAddress = macAddress;

        connectThread = ((MyApp)getApplicationContext()).getConnectionThreadInstance();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
        filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
        this.registerReceiver(mReceiver, filter);

        snippets = new ReadWriteSnippets(this);

        mLastActivity = bundle.getString(ACTIVITY);
        if (mLastActivity != null) {
            if (mLastActivity.equals(FingerPrintAuthActivity.TAG)) {
                handleLastSingnature();
            }
        }else {
            initNewSignature();
        }

        //Waiting for respond from desktop
        snippets.registerActionListener(userToken);

    }

    private void handleLastSingnature() {
        ActionStatus action = new ActionStatus(userToken, ActionsEnum.ANSWER, macAddress);
        //Write ANSWER to PENDING message
            snippets.writeAction(action);
    }

    private void initNewSignature() {
        ActionStatus action = new ActionStatus(userToken, ActionsEnum.WAITING, macAddress);
        //Write waiting to connect message
            snippets.writeAction(action);
    }

    public static void actionChanged(ActionStatus newAction, final Context context){
        actionText.setText(newAction.getActionStatus().toString());

        switch (newAction.getActionStatus()){
            case PENDING:
                Intent intent = new Intent(context, FingerPrintAuthActivity.class);
                Bundle bundle = new Bundle();

                bundle.putString(ACTIVITY, TAG);
                bundle.putString(MainActivity.MAC_ADDRESS, newAction.getMacAddress());
                bundle.putString(MainActivity.USER_TOKEN, newAction.getUserToken());
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getAppContext().startActivity(intent);

                break;

            case WAITING:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                break;

            case ANSWER:
                progressBar.setVisibility(ProgressBar.VISIBLE);
                break;

            case ABORT:
                progressBar.setVisibility(ProgressBar.GONE);
                try{
                    connectThread.cancel();
                }catch (RuntimeException e){
                    Log.d(TAG, "actionChanged: ABORT "+e.getMessage());
                }

                final Intent bluetoothIntent = new Intent(context, BluetoothActivity.class);
                Bundle bluetoothBundle = new Bundle();
                bluetoothBundle.putString(MainActivity.USER_TOKEN, newAction.getUserToken());
                bluetoothIntent.putExtras(bluetoothBundle);

                AlertDialog.Builder abortAlert = new AlertDialog.Builder(context);
                abortAlert.setTitle(R.string.problem_alert_title)
                .setMessage(R.string.problem_alert_message)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(bluetoothIntent);
                    }
                }).setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert)
                        .create().show();

                break;

            case CONNECTED:
                progressBar.setVisibility(ProgressBar.GONE);
                break;

            case DISCONNECT:
                progressBar.setVisibility(ProgressBar.GONE);
//                try{
//                    connectThread.cancel();
//                }catch (RuntimeException e){
//                    Log.d(TAG, "actionChanged: ABORT "+e.getMessage());
//                }
                break;
        }
    }

    public void onDisconnectClicked(View view) {
        disconnect();
    }

    private void disconnect() {
        final Intent bluetoothIntent = new Intent(this, BluetoothActivity.class);
        Bundle bluetoothBundle = new Bundle();
        bluetoothBundle.putString(MainActivity.USER_TOKEN, userToken);
        bluetoothIntent.putExtras(bluetoothBundle);

        AlertDialog.Builder abortAlert = new AlertDialog.Builder(this);
        abortAlert.setTitle(R.string.dissconnect_title)
                .setMessage(R.string.to_exit)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Unregister the receiver before disconnecting the thread
                        //in order to avoid duplicated dialogs
                        unregisterReceiver(mReceiver);
                        //Than disabling the bluetooth connection
                        connectThread.cancel();
                        snippets.deleteAllMessages(userToken);
                        snippets.writeAction(new ActionStatus(userToken,ActionsEnum.DISCONNECT,macAddress));
                        startActivity(bluetoothIntent);
                    }
                }).setNegativeButton(R.string.continue1, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .create().show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        snippets.deleteAllMessages(userToken);

    }

    @Override
    public void onBackPressed() {
        disconnect();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
        connectThread.cancel();

        snippets.writeAction(new ActionStatus(userToken, ActionsEnum.DISCONNECT, macAddress));
    }

    public static Context getAppContext(){
        return mContext;
    }

    //The BroadcastReceiver that listens for bluetooth broadcasts
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        private boolean first = true;
        @Override
        public void onReceive(final Context context, Intent intent) {
            String action = intent.getAction();
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

            if (BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED.equals(action)) {
            //Device is about to disconnect
                Toast.makeText(ActionActivity.getAppContext(), "Device is about to disconnect", Toast.LENGTH_SHORT).show();
            }
            else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
            //Device has disconnected
                Toast.makeText(ActionActivity.getAppContext(), "Device is disconnect!!!!!", Toast.LENGTH_SHORT).show();
                snippets.writeAction(new ActionStatus(userToken,ActionsEnum.DISCONNECT, macAddress));

                final Intent bluetoothIntent = new Intent(context, BluetoothActivity.class);
                Bundle bluetoothBundle = new Bundle();
                bluetoothBundle.putString(MainActivity.USER_TOKEN, userToken);
                bluetoothIntent.putExtras(bluetoothBundle);

                AlertDialog.Builder abortAlert = new AlertDialog.Builder(context);
                abortAlert.setTitle(R.string.problem_alert_title)
                        .setMessage(R.string.lost_connection)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                context.startActivity(bluetoothIntent);
                            }
                        }).setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert)
                        .create().show();
            }
        }
    };
}
