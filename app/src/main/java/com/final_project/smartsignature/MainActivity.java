package com.final_project.smartsignature;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    public final static String USER_TOKEN = "user_token";
    public final static String MAC_ADDRESS = "mac_address";
    public final static String TAG = "MainActivity";

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        mUser=mAuth.getCurrentUser();

        if(mUser != null){
            checkTokenSession();
        }else{
            initSigninUI();
        }

    }

    private void checkTokenSession() {
        mUser=mAuth.getCurrentUser();
        mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
            @Override
            public void onComplete(@NonNull Task<GetTokenResult> task) {
                if (task.isSuccessful()) {
                    long expTimeSession = task.getResult().getAuthTimestamp() + 3600;

                    if (expTimeSession < System.currentTimeMillis() / 1000) {
                        Log.d(TAG, "onComplete: Time session is occurred");
                        initSigninUI();
                    }
                    else {
                        Log.d(TAG, "onComplete: Time session is on!");
                        signin(task.getResult().getToken());
                    }
                }else
                    Log.e(TAG, "onComplete: TaskUnsuccessful", task.getException());
            }
        });
    }

    private void signin(String token) {
        Toast.makeText(this, "Sign In Success", Toast.LENGTH_SHORT).show();
        //Open the finger BluetoothActivity activity - just for test!!
        Intent intent = new Intent(this, FingerPrintAuthActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(ActionActivity.ACTIVITY, TAG);

        startActivity(intent.putExtra(USER_TOKEN, token));
    }

    private void initSigninUI() {
        //Do not allow to create new users
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().setAllowNewAccounts(false).build());

        // Create and launch sign-in intent
        startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setIsSmartLockEnabled(false)
                        .build(),
                RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                String idToken = "";
                // Successfully signed in
                Log.d("MainActivity", "Successfully signed in: ");
                checkTokenSession();

            } else {
                Toast.makeText(this, "Sign In failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
}