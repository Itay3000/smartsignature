package com.final_project.smartsignature;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ToggleButton;

import com.final_project.smartsignature.bluetooth_handlers.ConnectThread;
import com.final_project.smartsignature.bluetooth_handlers.DeviceItem;

import java.util.ArrayList;
import java.util.List;

public class BluetoothActivity extends AppCompatActivity implements ReadWriteSnippets.MacAddressConnection {
    public static String TAG = "BluetoothActivity";
    public static String DEVICE = "DeviceConnection";
    private BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    private final List<DeviceItem> devices= new ArrayList<>();
    private ArrayAdapter<DeviceItem> mAdapter;
    private ListView listView;
    private String user_token;
    private ReadWriteSnippets snippets;
    private ArrayList<String> macAddresses;
    private ProgressBar progressBar;
    private boolean secondAccess;
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
//                // Discovery has found a device. Get the BluetoothDevice
//                // object and its info from the Intent.
//                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
//                String deviceInfo=device.getName() + "\n" + device.getAddress();

//                devices.add(deviceInfo);
//                mAdapter.notifyDataSetChanged();
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                // Create a new device item
                DeviceItem newDevice = new DeviceItem(device.getName(), device.getAddress(), false);
                if(newDevice.getDeviceName() != null && macAddresses.contains(newDevice.getSpecialMacAddress())){
                    Log.i(TAG, "onReceive: Found new Device!#n"+device.getName());
                    // Add it to our adapter
                    mAdapter.add(newDevice);
                }
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action))
                Log.i(TAG, "onReceive: Start discovering");
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
                Log.i(TAG, "onReceive: Discovery finished");

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);

        progressBar = findViewById(R.id.bluetooth_pb);
        user_token = getIntent().getStringExtra(MainActivity.USER_TOKEN);
        snippets = new ReadWriteSnippets(this);
        // Phone does not support Bluetooth so let the user know and exit.
        if (bluetoothAdapter == null) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.not_compatible)
                    .setMessage(R.string.no_bluetooth)
                    .setPositiveButton("Exit", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            System.exit(0);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .create().show();
        }
        //Bluetooth is off
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBT, 1);
            bluetoothAdapter.enable();
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1111);
        }

        snippets.registerMacAddressesListener(this);

        mAdapter = new ArrayAdapter<DeviceItem>(this, android.R.layout.simple_list_item_1, devices);
        listView = (ListView) findViewById(R.id.device_list);
        listView.setAdapter(mAdapter);
        ToggleButton scan = findViewById(R.id.scan);

        scan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
                if (isChecked) {
                    mAdapter.clear();
                    registerReceiver(receiver, filter);
                    bluetoothAdapter.startDiscovery();

                } else {
                    unregisterReceiver(receiver);
                    bluetoothAdapter.cancelDiscovery();
                }
            }
        });

        setBluetoothListOnClick();
    }

    private void setBluetoothListOnClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DeviceItem device = ((DeviceItem)parent.getItemAtPosition(position));
                Log.e("TAG","Device clicked: " + device.getDeviceName());

                makeNewConnection(device);
            }
        });
    }

    private void makeNewConnection(DeviceItem deviceItem) {
        //Make a bluetooth connection
        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(deviceItem.getMacAddress());
        ConnectThread connectThread = ((MyApp)getApplicationContext()).newConnectionThreadInstance(device);


        try {
            connectThread.run();
            //Switch to the connection action activity
            Intent intent = new Intent(this, ActionActivity.class);
            Bundle bundle = new Bundle();

            bundle.putString(MainActivity.MAC_ADDRESS, deviceItem.getSpecialMacAddress());
            bundle.putString(MainActivity.USER_TOKEN, user_token);

            intent.putExtras(bundle);

            startActivity(intent);
        }catch (RuntimeException e){
            Log.e(TAG, "makeNewConnection error: "+e.getMessage());
            AlertDialog.Builder abortAlert = new AlertDialog.Builder(this);
            abortAlert.setTitle(R.string.problem_alert_title)
                    .setMessage(R.string.connection_problem)
                    .setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            return;
                        }
                    }).setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.exit(0);
                }
            }).setIcon(android.R.drawable.ic_dialog_alert)
                    .create().show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Unregister the bluetooth listener
        unregisterReceiver(receiver);
    }

    @Override
    public void onCallBack(ArrayList<String> macAddresses) {
        this.macAddresses = macAddresses;

        progressBar.setVisibility(ProgressBar.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, MainActivity.class));
    }
}

