package com.final_project.smartsignature;

public enum ActionsEnum {
    CONNECTED,      //Connected to the system (Catches - WAITING & PENDING messages) - Desktop message
    DISCONNECT,   //Normal exit (After the time session ends or the user wants to exit) - Android message
    ABORT,          // Can't access for any reason (Wrong token / race between users etc) - Desktop message
    PENDING,        //Pending for sign (after the access is approved) - Desktop message
    WAITING,         //Waiting to gain an access (The first message) - Android message
    ANSWER,         //Answer to pending message - Android message (When the desktop get the message it returns CONNECTED)
    ERROR           //Some kind of error - Both message
}

