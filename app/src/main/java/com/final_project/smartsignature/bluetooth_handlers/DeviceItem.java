package com.final_project.smartsignature.bluetooth_handlers;

import android.text.TextUtils;

import java.io.Serializable;

public class DeviceItem implements Serializable {
    private String deviceName;
    private String macAddress;
    private boolean connected;

    public DeviceItem(String deviceName, String macAddress, boolean connected) {
        this.deviceName = deviceName;
        this.macAddress = macAddress;
        this.connected = connected;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public  String getSpecialMacAddress() {
        return TextUtils.join("", macAddress.split(":"));
    }

    @Override
    public String toString() {
        return this.deviceName;
    }
}
