package com.final_project.smartsignature.bluetooth_handlers;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class MacAddress {
    String macAddress;

    public MacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public MacAddress(){}

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("macAddress", macAddress);

        return result;
    }
}
