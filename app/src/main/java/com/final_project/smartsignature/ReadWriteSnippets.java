package com.final_project.smartsignature;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.final_project.smartsignature.bluetooth_handlers.MacAddress;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class ReadWriteSnippets  {
    public static String TAG = "ReadWriteSnippets";
    public static String ACTION_STATUSES = "action_statuses";
    public static String MAC_ADDRESS = "mac_address";

    private Context mContext;

    private FirebaseDatabase mFirebase;
    private DatabaseReference mDatabase;

    public ReadWriteSnippets(Context context) {
        mFirebase = FirebaseDatabase.getInstance();
        this.mContext = context;
    }

    public void writeAction(final ActionStatus actionStatus){

        //First delete the last computer message - than write new action
        mDatabase = mFirebase.getReference(ACTION_STATUSES);
        mDatabase.orderByChild("userToken").equalTo(actionStatus.getUserToken()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        if (child.exists()) {
                            Log.i(TAG, "onDataChange: Key of delete " + child.getKey());
                            ActionsEnum actionStatus = child.getValue(ActionStatus.class).getActionStatus();
                            if(actionStatus.equals(ActionsEnum.ABORT)||
                                    actionStatus.equals(ActionsEnum.PENDING) || actionStatus.equals(ActionsEnum.WAITING))
                                child.getRef().removeValue();
                        }
                    }

                }
                writeNewAction(actionStatus);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void writeNewAction(ActionStatus actionStatus) {
        mDatabase = mFirebase.getReference();
        String key = mDatabase.push().getKey();

        Map<String, Object> actionValues = actionStatus.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/" + ACTION_STATUSES + "/" + key, actionValues);

        mDatabase.updateChildren(childUpdates).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                // Write was successful!
                Toast.makeText(mContext,"Starting connection...",Toast.LENGTH_SHORT).show();
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Write failed
                        Toast.makeText(mContext,"Couldn't make a connection...",Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onFailure: "+e.getMessage(), e);
                    }
                });
    }

    public void deleteAllMessages(String userToken) {
        mDatabase = mFirebase.getReference(ACTION_STATUSES);
        mDatabase.orderByChild("userToken").equalTo(userToken).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot child : dataSnapshot.getChildren()) {
                        if (child.exists()) {
                            Log.i(TAG, "onDataChange: Key of delete " + child.getKey());
                            String actionStatus = (String) child.child("actionStatus").getValue();
                                child.getRef().removeValue();
                        }
                    }
                }else
                    return;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void registerActionListener(final String myToken){
        mDatabase=mFirebase.getReference(ACTION_STATUSES);

        mDatabase.orderByChild("userToken").equalTo(myToken).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data: dataSnapshot.getChildren()){
                    Log.d(TAG, "onDataChange:" + data.getKey());
                    ActionStatus value = data.getValue(ActionStatus.class);
//                Toast.makeText(mContext,value.getActionStatus().toString(),Toast.LENGTH_SHORT).show();
                    ActionActivity.actionChanged(value, mContext);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void registerMacAddressesListener(final MacAddressConnection listener){
        final ArrayList<String> macAddresses = new ArrayList<>();
        mDatabase = mFirebase.getReference(MAC_ADDRESS);


        mDatabase.orderByChild("macAddress").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for(DataSnapshot data: dataSnapshot.getChildren()){
                        String value = data.getValue(MacAddress.class).getMacAddress();
                        Log.d(TAG, "onDataChange: mac address ->" + value);
                        macAddresses.add(value);
                    }

                    listener.onCallBack(macAddresses);
                    Log.i(TAG, "onDataChange: Mac address search firebase finished");
                }
                else {

                    Log.i(TAG, "onDataChange: Mac address NOT found");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    public interface  MacAddressConnection {
        void onCallBack(ArrayList<String> macAddresses);
    }

}
